package Main;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import com.exercise.library.Person;
import com.exercise.library.User;

public class Main {

	public static void main(String[] args) {
		
		Date date = new Date();
		Person p = new Person();
		User u = new User();
		p.setDateOfBirth(date);
		p.setEmail("person_person@person.com");
		p.setFirstName("FirstName");
		u.setLogin("login");
		p.setNip("11111111111");
		u.setPassword("password");
		p.setPesel("94111902236");
		p.setSurname("Surname");
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(p);
		session.save(u);
		session.getTransaction().commit();
		
	}

}