package repository;

import java.util.List;

import com.exercise.library.User;

public interface IUserRepository extends IRepository<User> {
    
	User withLogin(String login);
    User withLoginAndPassword(String login, String password);
    User setupPermissions(User user);
    User setupRoles(User user);
	void save(User def);
	List<User> getAll();
}