package repository;

public interface IRepository<TEntity> {

    void add(TEntity entity);
    void delete(TEntity entity);
    void modify(TEntity entity);
	void persistUpdate(TEntity entity);
	void persistDelete(TEntity entity);
	void persistAdd(TEntity entity);
}
