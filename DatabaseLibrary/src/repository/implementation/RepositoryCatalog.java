package repository.implementation;

import java.sql.Connection;

import builder.implementation.EnumerationValueBuilder;
import builder.implementation.PersonBuilder;
import builder.implementation.UserBuilder;
import repository.IEnumerationValueRepository;
import repository.IPersonRepository;
import repository.IRepositoryCatalog;
import repository.IUserRepository;
import unitOfWorkRepository.implementation.UnitOfWork;

public class RepositoryCatalog implements IRepositoryCatalog {

    private Connection connection;
    private UnitOfWork unitOfWork;

    public RepositoryCatalog(Connection connection, UnitOfWork unitOfWork) {
        this.connection = connection;
        this.unitOfWork = unitOfWork;
    }

    public IEnumerationValueRepository enumeration() {
        return new EnumerationValueRepository(this.connection, new EnumerationValueBuilder(), this.unitOfWork);
    }

    public IUserRepository users() {
        return new UserRepository(this.connection, new UserBuilder(), this.unitOfWork);
    }

    public IPersonRepository persons() {
        return new PersonRepository(this.connection, new PersonBuilder(), this.unitOfWork);
    }
}