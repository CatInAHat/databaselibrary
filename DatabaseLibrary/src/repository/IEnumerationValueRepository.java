package repository;

import com.exercise.library.EnumerationValue;

public interface IEnumerationValueRepository extends IRepository<EnumerationValue> {
    
	EnumerationValue withName(String name);
    EnumerationValue withStringKey(String key);
}