package repository;

import com.exercise.library.Person;

public interface IPersonRepository extends IRepository<Person>{
    
	Person withFirstName(String firstName);
    Person withLastName(String lastName);
}