package repository;

public interface IRepositoryCatalog {
	
    IEnumerationValueRepository enumeration();
    IUserRepository users();
    IPersonRepository persons();
}