package builder.implementation;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.exercise.library.EnumerationValue;

import builder.IEntityBuilder;


public class EnumerationValueBuilder implements IEntityBuilder<EnumerationValue> {
    
	public EnumerationValue build(ResultSet rs) throws SQLException {
        
    	EnumerationValue enumerationValue = new EnumerationValue();

        enumerationValue.setStringKey(rs.getString("key"));
        enumerationValue.setEnumerationName(rs.getString("name"));
        enumerationValue.setValue(rs.getString("value"));

        return enumerationValue;
    }
}