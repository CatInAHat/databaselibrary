package builder.implementation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;

import com.exercise.library.Person;

import builder.IEntityBuilder;

public class PersonBuilder implements IEntityBuilder<Person> {
    public Person build(ResultSet rs) throws SQLException {
        Person person = new Person();

        person.setFirstName(rs.getString("first_name"));
        person.setSurname(rs.getString("surname"));
        person.setPesel(rs.getString("pesel"));
        person.setNip(rs.getString("nip"));
        person.setEmail(rs.getString("email"));
        try {
            person.setDateOfBirth(DateFormat.getDateInstance().parse(rs.getString("date_of_birth")));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return person;
    }
}