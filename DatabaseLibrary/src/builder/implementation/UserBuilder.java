package builder.implementation;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.exercise.library.User;

import builder.IEntityBuilder;


public class UserBuilder implements IEntityBuilder<User> {
    public User build(ResultSet rs) throws SQLException {
       
    	User user = new User();
        user.setId(rs.getString("id"));
        user.setLogin(rs.getString("login"));
        user.setPassword(rs.getString("password"));
        return user;
    }
}