package builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.exercise.library.Entity;

public interface IEntityBuilder<TEntity extends Entity> {
    
	TEntity build(ResultSet rs) throws SQLException;
}
