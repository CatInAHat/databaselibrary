package unitOfWorkRepository.implementation;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import com.exercise.library.Entity;
import com.exercise.library.EntityState;

import unitOfWorkRepository.IUnitOfWork;
import unitOfWorkRepository.IUnitOfWorkRepository;

public class UnitOfWork implements IUnitOfWork {

    private Map<Entity, IUnitOfWorkRepository> entities = new LinkedHashMap<Entity, IUnitOfWorkRepository>();
    private Connection connection;

    public void saveChanges() {

        for(Entity entity: entities.keySet())
        {
            switch(entity.getState())
            {
                case Modified:
                    entities.get(entity).persistUpdate(entity);
                    break;
                case Deleted:
                    entities.get(entity).persistDelete(entity);
                    break;
                case New:
                    entities.get(entity).persistAdd(entity);
                    break;
                case UnChanged:
                    break;
                default:
                    break;}
        }

        try {
            connection.commit();
            entities.clear();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void undo() {
        entities.clear();
    }

    public void markAsNew(Entity entity, IUnitOfWorkRepository repo) {
        entity.setState(EntityState.New);
        entities.put(entity, repo);
    }

    public void markAsDeleted(Entity entity, IUnitOfWorkRepository repo) {
        entity.setState(EntityState.Deleted);
        entities.put(entity, repo);
    }

    public void markAsChanged(Entity entity, IUnitOfWorkRepository repo) {
        entity.setState(EntityState.Modified);
        entities.put(entity, repo);
    }
}