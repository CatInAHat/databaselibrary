package unitOfWorkRepository;

public interface IUnitOfWorkRepository<TEntity> {
    
	void persistAdd(TEntity entity);
    void persistDelete(TEntity entity);
    void persistUpdate(TEntity entity);
	void modify(TEntity entity);
	void delete(TEntity entity);
	void add(TEntity entity);
}