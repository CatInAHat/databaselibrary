package unitOfWorkRepository;

import com.exercise.library.Entity;

public interface IUnitOfWork {
    
	void saveChanges();

    void undo();

    void markAsNew(Entity entity, IUnitOfWorkRepository repo);

    void markAsDeleted(Entity entity, IUnitOfWorkRepository repo);

    void markAsChanged(Entity entity, IUnitOfWorkRepository repo);
}