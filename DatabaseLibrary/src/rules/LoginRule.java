package rules;

import com.exercise.library.User;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;

public class LoginRule implements ICanCheckRule<User> {
    public CheckResult checkRule(User entity) {

        if (entity.getLogin() == null)
            return new CheckResult("", RuleResult.Error);
        if (entity.getLogin().equals(""))
            return new CheckResult("", RuleResult.Error);
        return new CheckResult("", RuleResult.Ok);
    }
}