package rules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;

import com.exercise.library.User;

public class PasswordRule implements ICanCheckRule<User>{

	String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{5,10}"; //has�o musi zawiera� (od lewej): cyfr�, ma�� liter�, du��, znak, brak spacji
	
	public CheckResult checkRule(User entity) {

		if(entity.getPassword()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getPassword().equals(""))
			return new CheckResult("", RuleResult.Error);
		if(entity.getPassword().matches(pattern))
			return new CheckResult("", RuleResult.Error);
		return new CheckResult("", RuleResult.Ok);
	}

}
