package rules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;

import com.exercise.library.Person;

public class DateRule implements ICanCheckRule<Person>{

	public CheckResult checkRule(Person entity) {

		if(entity.getDateOfBirth()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getDateOfBirth().equals(""))
			return new CheckResult("", RuleResult.Error);
		if(entity.getDateOfBirth().before(java.sql.Date.valueOf("2015-11-25")))
			return new CheckResult("", RuleResult.Error);
		return new CheckResult("", RuleResult.Ok);
	}

}