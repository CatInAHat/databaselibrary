package rules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;

import com.exercise.library.Person;

public class NipRule implements ICanCheckRule<Person>{
	
		public CheckResult checkRule(Person entity) {				

	        int[] weights = {6, 5, 7, 2, 3, 4, 5, 6, 7};
	        int sum = 0;
	        if (entity.getNip() != null) {
	            if (!entity.getNip().equals("")) {
	                for (int i = 0; i < weights.length; i++) {
	                    sum += Integer.parseInt(entity.getNip().substring(i, i + 1)) * weights[i];
	                }
	            }
	        }
		    
			if(entity.getNip()==null)
				return new CheckResult("", RuleResult.Error);
			if(entity.getNip().equals(""))
				return new CheckResult("", RuleResult.Error);
			if(entity.getNip().equals(""))
				return new CheckResult("", RuleResult.Error);
			if(entity.getNip().length() != 10)
				return new CheckResult("", RuleResult.Error);
			if(sum % 11 == 10)
				return new CheckResult("", RuleResult.Error);
			
			return new CheckResult("", RuleResult.Ok);
		}
}
