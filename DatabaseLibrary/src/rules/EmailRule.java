package rules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;

import com.exercise.library.Person;

public class EmailRule implements ICanCheckRule<Person> {

	private static final String EMAIL_PATTERN = 				// email musi mie� og�lnie przyj�ty wz�r emaila
		"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";	
	
	public CheckResult checkRule(Person entity) {

		if(entity.getEmail()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getEmail().equals(""))
			return new CheckResult("", RuleResult.Error);
		if(entity.getEmail().equals(EMAIL_PATTERN))
			return new CheckResult("", RuleResult.Error);
		return new CheckResult("", RuleResult.Ok);
	}

}
