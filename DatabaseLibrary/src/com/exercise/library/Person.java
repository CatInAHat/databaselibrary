package com.exercise.library;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "T_P_PERSON")
public class Person extends Entity{

	 @Column(name = "first_name")
	 private String firstName;
     @Column(name = "surname")
     private String surname;
     @Column(name = "pesel")
     private String pesel;
     @Column(name = "nip")
     private String nip;
     @Column(name = "email")
     private String email;
     @Column(name = "date_of_birth")
     private Date dateOfBirth;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getPesel() {
		return pesel;
	}
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	public String getNip() {
		return nip;
	}
	public void setNip(String nip) {
		this.nip = nip;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
}
