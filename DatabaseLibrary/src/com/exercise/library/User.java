package com.exercise.library;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "T_SYS_USERS")
public class User extends Entity {
	
	@Column(name = "login")
	private String login;
	@Column(name = "password")
	private String password;
	
	private List<UserRoles> userRoles = new ArrayList<UserRoles>();
	private List<RolesPermissions> rolesPermissions = new ArrayList<RolesPermissions>();
	private List<Address> address = new ArrayList<Address>();
	private List<PhoneNumber> phoneNumber = new ArrayList<PhoneNumber>();
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<UserRoles> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<UserRoles> userRoles) {
		this.userRoles = userRoles;
	}

	public List<RolesPermissions> getRolesPermissions() {
		return rolesPermissions;
	}

	public void setRolesPermissions(List<RolesPermissions> rolesPermissions) {
		this.rolesPermissions = rolesPermissions;
	}

	public List<Address> getAddress() {
		return address;
	}

	public void setAddress(List<Address> address) {
		this.address = address;
	}

	public List<PhoneNumber> getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(List<PhoneNumber> phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void login() {
		
	};
	
	public void password() {
		
	};
}
