package com.exercise.library;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import repository.implementation.RepositoryCatalog;
import unitOfWorkRepository.implementation.UnitOfWork;

import com.exercise.library.EnumerationValue;

import cache.StoringCache;

import com.exercise.library.User;

public class Application {
    public static void main(String[] args) {
        
    	EnumerationValue sysEnum1 = new EnumerationValue();
        EnumerationValue sysEnum2 = new EnumerationValue();

        StoringCache<EnumerationValue> cacheStorage = new StoringCache<>();

        cacheStorage.put(sysEnum1.getEnumerationName(), sysEnum1);
        cacheStorage.put(sysEnum2.getEnumerationName(), sysEnum2);

        System.out.println(cacheStorage.size());

        cacheStorage.cleanup();
        System.out.println(cacheStorage.size());

        try {
            Thread.sleep(6000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        cacheStorage.cleanup();
        System.out.println(cacheStorage.size());

        String databaseUrl = "jdbc:posgresql://localhost:5432/hibernatedb";

        User def = new User();
        def.setLogin("postgre");
        def.setPassword("unique");

        try {
            Class.forName("org.postgresql.Driver");
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        try {
            Connection connection = DriverManager.getConnection(databaseUrl);
            UnitOfWork unitOfWork = new UnitOfWork();
            RepositoryCatalog repositoryCatalog = new RepositoryCatalog(connection, unitOfWork);

            repositoryCatalog.users().save(def);
            unitOfWork.saveChanges();

            List<User> usersFromDatabase = repositoryCatalog.users().getAll();
            for (User user : usersFromDatabase) {
                System.out.println(user.getId() + " " + user.getLogin() + " " + user.getPassword());
            }

            System.out.println(repositoryCatalog.users().count());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}