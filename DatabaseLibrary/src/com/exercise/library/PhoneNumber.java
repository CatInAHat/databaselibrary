package com.exercise.library;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "T_P_PHONE")
public class PhoneNumber extends Entity {

	    @Column(name = "country_prefix")
	    private String countryPrefix;
	    @Column(name = "city_prefix")
	    private String cityPrefix;
	    @Column(name = "number")
	    private String number;
	    @Column(name = "type_id")
	    private int typeId;
	
	public String getCountryPrefix() {
		return countryPrefix;
	}
	public void setCountryPrefix(String countryPrefix) {
		this.countryPrefix = countryPrefix;
	}
	public String getCityPrefix() {
		return cityPrefix;
	}
	public void setCityPrefix(String cityPrefix) {
		this.cityPrefix = cityPrefix;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
}
