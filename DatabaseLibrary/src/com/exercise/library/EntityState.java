package com.exercise.library;

public enum EntityState {
	New, Modified, UnChanged, Deleted, Unknown
}
