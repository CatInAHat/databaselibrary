package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import rules.EmailRule;
import checker.CheckResult;
import checker.RuleResult;

import com.exercise.library.Person;

public class EmailRuleTest {

EmailRule rule = new EmailRule();
	
	@Test
	public void checker_should_check_if_the_person_email_is_not_null(){
		Person p = new Person();
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_person_email_is_not_empty(){
		Person p = new Person();
		p.setEmail("");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_OK_if_the_email_is_not_null(){
		Person p = new Person();
		p.setEmail("example@example");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}
	
	@Test
	public void checker_should_return_Ok_if_the_email_has_email_pattern(){
		Person p = new Person();
		p.setEmail("example@example.pl");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}

}
