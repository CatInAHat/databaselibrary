package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import rules.PasswordRule;
import checker.CheckResult;
import checker.RuleResult;

import com.exercise.library.User;

public class PasswordRuleTest {

	PasswordRule rule = new PasswordRule();
	
	@Test
	public void checker_should_check_if_the_user_password_is_not_null(){
		User u = new User();
		CheckResult result =rule.checkRule(u);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_user_password_is_not_empty(){
		User u = new User();
		u.setPassword("");
		CheckResult result =rule.checkRule(u);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_OK_if_the_password_is_not_null(){
		User u = new User();
		u.setPassword("example");
		CheckResult result =rule.checkRule(u);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}
	
	@Test
	public void checker_should_check_if_the_password_matches_password_pattern(){
		User u = new User();
		u.setPassword("Password#1");
		CheckResult result =rule.checkRule(u);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}

}
