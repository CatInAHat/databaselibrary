package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;

import com.exercise.library.User;

import rules.LoginRule;


public class LoginRuleTest {
    
	LoginRule rule = new LoginRule();

    @Test
    public void checker_should_check_if_the_user_login_is_not_null() {
        User u = new User();
        CheckResult result = rule.checkRule(u);
        assertTrue(result.getResult().equals(RuleResult.Error));

    }

    @Test
    public void checker_should_check_if_the_user_login_is_not_empty() {
        User u = new User();
        u.setLogin("");
        CheckResult result = rule.checkRule(u);
        assertTrue(result.getResult().equals(RuleResult.Error));

    }

    @Test
    public void checker_should_return_Ok_if_the_login_is_not_null() {
        User u = new User();
        u.setLogin("User123");
        CheckResult result = rule.checkRule(u);
        assertTrue(result.getResult().equals(RuleResult.Ok));

    }
}