package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import rules.DateRule;
import checker.CheckResult;
import checker.RuleResult;

import com.exercise.library.Person;

public class DateRuleTest {

DateRule rule = new DateRule();
	
	@Test
	public void checker_should_check_if_the_person_DOB_is_not_null(){
		Person p = new Person();
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_person_DOB_is_not_empty(){
		Person p = new Person();
		p.setDateOfBirth(null);
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_DOB_is_before_current_date(){
		Person p = new Person();
		p.setDateOfBirth(java.sql.Date.valueOf("2013-09-04"));
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}

}
