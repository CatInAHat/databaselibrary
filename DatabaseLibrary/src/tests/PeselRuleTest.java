package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import rules.PeselRule;

import com.exercise.library.Person;

public class PeselRuleTest {

	PeselRule rule = new PeselRule();
	
	@Test
	public void checker_should_check_if_the_person_pesel_is_not_null(){
		Person p = new Person();
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_person_pesel_is_not_empty(){
		Person p = new Person();
		p.setPesel("");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_Ok_if_person_pesel_has_correct_number_of_chars(){
		Person p = new Person();
		p.setPesel("94111902236");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}
	
	@Test
	public void checker_should_check_if_person_pesel_is_correct(){
		Person p = new Person();
		p.setPesel("94111902236");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}
	

}
