package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import rules.NipRule;
import checker.CheckResult;
import checker.RuleResult;

import com.exercise.library.Person;

public class NipRuleTest {

NipRule rule = new NipRule();
	
	@Test
	public void checker_should_check_if_the_person_nip_is_not_null(){
		Person p = new Person();
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_person_nip_is_not_empty(){
		Person p = new Person();
		p.setFirstName("");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_Ok_if_the_nip_has_correct_number_of_char(){
		Person p = new Person();
		p.setNip("8771944298");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}
	
	@Test
	public void checker_should_return_OK_if_the_nip_is_not_null(){
		Person p = new Person();
		p.setNip("8771944298");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}
	
	@Test
	public void checker_should_return_OK_if_the_nip_is_valid(){
		Person p = new Person();
		p.setNip("8771944298");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}
}
