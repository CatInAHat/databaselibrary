package cache;

import java.util.ArrayList;
import java.util.HashMap;

import com.exercise.library.EnumerationValue;


public class Cache{
	
	private static Cache instance;
	private HashMap<String, ArrayList<EnumerationValue>> cacheObjects = new HashMap<String, ArrayList<EnumerationValue>>();
	private HashMap<String, ArrayList<EnumerationValue>> cacheUpdated = new HashMap<String, ArrayList<EnumerationValue>>();
	
	private Cache(){		
	}
	public static Cache getInstance(){
		if (instance==null) 
			instance = new Cache();
		return instance;
	}
	public void clean(){
		cacheUpdated.clear();
	}
	public void put(EnumerationValue element){
		if (! (cacheUpdated.containsKey(element.getEnumerationName()))){
			cacheUpdated.put(element.getEnumerationName(), new ArrayList<EnumerationValue>());
		}
		cacheUpdated.get(element.getEnumerationName()).add(element);
	}
	public void finalizeUpdate(){
		cacheObjects = cacheUpdated;
	}	
	public ArrayList<EnumerationValue> get(String key){
		return cacheObjects.get(key);
	}
}